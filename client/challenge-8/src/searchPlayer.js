import React, { Component } from 'react'

export default class searchPlayer extends Component {
    render() {
        return (
            <body>
                <div class="container my-5">
                    <div class="card p-5">
                        <center>
                            <h1>Search Player</h1>
                        </center>
                        <form action="/api/v1/articles" method="POST">
                            <div class="my-3">
                                <label for="keyword" class="form-label h5">Keyword</label>
                                <input type="text" class="form-control" id="keyword" name="keyword" aria-describedby="keyword"/>
                            </div>
                        <select class="form-select my-3" aria-label="Default select example">
                            <option value="username" selected>Username</option>
                            <option value="email">Email</option>
                            <option value="exp">Experience</option>
                            <option value="lvl">Level</option>
                        </select>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </body>

        )
    }
}
